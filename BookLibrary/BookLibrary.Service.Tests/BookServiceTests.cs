﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookLibrary.Services;
using BookLibrary.Services.Mapper;
using BookLibrary.Services.Models;
using BookLibrary.Services.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NUnit.Framework;

namespace BookLibrary.Service.Tests
{
    
    public class BookServiceTests
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapperService _mapperService;
        private readonly IBookService _bookService;


        public BookServiceTests()
        {
            _bookRepository = Substitute.For<IBookRepository>();
            _mapperService= Substitute.For<IMapperService>();
            _bookService = new BookService(_bookRepository, _mapperService);
        }

        [Test]
        public void BookService_GetAll_Should_Invoke_BookRepository_FindAll_To_Fetch_ResultSet()
        {
            var mockBooks = new List<BookEntity>();
            _bookRepository.FindAll().Returns(mockBooks.ToList());
            _bookService.GetAllBooks();
            _bookRepository.Received().FindAll();
        }

        [Test]
        public void BookService_GetByCategory_Should_Invoke_BookRepository_GetByCategory_To_Fetch_ResultSet()
        {
            var mockBooks = new List<BookEntity>();
            _bookRepository.GetByCategory("mockCategory").Returns(mockBooks.ToList());
            _bookService.GetByCategory("mockCategory");
            _bookRepository.Received().GetByCategory("mockCategory");
        }
        [Test]
        public void BookService_GetCategories_Should_Invoke_BookRepository_GetAllCategories_To_Fetch_ResultSet()
        {
            var mockData = new List<string>();
            _bookRepository.GetAllCategories().Returns(mockData.ToList());
            _bookService.GetCategories();
            _bookRepository.Received().GetAllCategories();
        }

        [Test]
        public void BookService_Search_Should_Invoke_BookRepository_Search_To_Fetch_ResultSet()
        {
            var mockData = new List<BookEntity>();
            _bookRepository.Search("mockSearchKey").Returns(mockData);
            _bookService.Search("mockSearchKey");
            _bookRepository.Received().Search("mockSearchKey");
        }
    }
}
