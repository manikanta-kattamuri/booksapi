﻿using System;
using System.Collections.Generic;
using StructureMap;


namespace BookLibrary.Web.Container
{
    public static class ContainerManager
    {
        private static volatile IContainer _current;
        private static readonly object SyncRoot = new Object();
        

        /// <summary>
        /// Gets the current instance of the container (thread safe)
        /// </summary>
        public static IContainer Current
        {
            get
            {
                if (_current == null)
                {
                    InitializeContainer(ObjectFactory.Container);
                }

                return _current;
            }
        }

        private static void InitializeContainer(object container)
        {
            throw new NotImplementedException();
        }

        internal static void InitializeContainer(IContainer container)
        {
            lock (SyncRoot)
            {
                _current = container;
            }


            container.Configure(c => c.Scan(s =>
            {
                s.Assembly("BookLibrary.Services");
                s.Assembly("BookLibrary.Web");
                s.LookForRegistries();
               
                s.TheCallingAssembly();
                s.WithDefaultConventions();
                //s.With(new ControllerConvention());

            }));
        }
    }
}