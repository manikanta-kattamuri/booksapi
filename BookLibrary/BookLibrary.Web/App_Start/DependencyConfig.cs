﻿using BookLibrary.Web.Dependency;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace BookLibrary.Web.App_Start
{
    public class DependencyConfig
    {
        public static void Config(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(config));
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

        }

        public static void CleanUp()
        {
            HttpContextLifecycle.DisposeAndClearAll();
        }
    }
}