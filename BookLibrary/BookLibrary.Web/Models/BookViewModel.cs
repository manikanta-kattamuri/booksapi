﻿
using BookLibrary.Services.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Web.Models
{
    public class BookViewModel
    {
        public List<Book> BookList { get; set; }

        [Required(ErrorMessage = "Category is required.")]
        public string SelectedCategory { get; set; }
        [Required(ErrorMessage = "Search text cannot be empty")]
        [MinLength(3,ErrorMessage = "Search text must be alteast 3 characters")]
        public string SearchString { get; set; }

        public IEnumerable<string> Categories { get; set; }
    }
}