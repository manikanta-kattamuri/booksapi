﻿using BookLibrary.Web.Models;
using System.Linq;
using System.Web.Mvc;
using BookLibrary.Services;

namespace BookLibrary.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBookService _bookService;

        public HomeController(IBookService bookService)
        {
            _bookService = bookService;
        }

        public ActionResult Index()
        {

            var books = _bookService.GetAllBooks();

            return View(new BookViewModel()
            {
                BookList = books.ToList(),
                Categories = _bookService.GetCategories()
            });
        }

        public ActionResult Search(BookViewModel bookViewModel)
        {
            var books = _bookService.Search(bookViewModel.SearchString);


            return View("Index", new BookViewModel()
            {
                BookList = books.ToList(),
                Categories = _bookService.GetCategories()
            });
        }

        public ActionResult Filter(BookViewModel bookViewModel)
        {
            var books = _bookService.GetByCategory(bookViewModel.SelectedCategory);



            return View("Index", new BookViewModel()
            {
                BookList = books.ToList(),
                Categories = _bookService.GetCategories()
            });
        }


        public ActionResult DisplayFilterSearch()
        {
            return PartialView("_FilterSearch", new BookViewModel()
            {

                Categories = _bookService.GetCategories()
            }); ;
        }
    }
}