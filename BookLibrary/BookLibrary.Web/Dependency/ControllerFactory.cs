﻿using BookLibrary.Services.Registries;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookLibrary.Web
{
    public class ControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, System.Type controllerType)
        {
            if (controllerType == null)
            {
                return base.GetControllerInstance(requestContext, controllerType);
            }
            return (Controller)ObjectFactory.GetInstance(controllerType);
        }

        public static class Bootstrapper
        {
            public static void Run()
            {
                ControllerBuilder.Current
                    .SetControllerFactory(new ControllerFactory());

                ObjectFactory.Initialize(x =>
                {
                    // x.AddConfigurationFromXmlFile("StructureMap.xml");
                    x.IncludeRegistry<ServiceRegistry>();
                });
            }
        }
    }

}
