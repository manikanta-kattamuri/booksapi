﻿using BookLibrary.Services.Mapper;
using BookLibrary.Services.Models;
using BookLibrary.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapperService _mapperService;

        public BookService(IBookRepository bookRepository,IMapperService mapperService)
        {
            _bookRepository = bookRepository;
            _mapperService = mapperService;
        }
        public IEnumerable<Book> GetAllBooks()
        {
            return _mapperService.Map<BookEntity, Book>(_bookRepository.FindAll());
        }

        public IEnumerable<Book> GetByCategory(string category)
        {
            return _mapperService.Map<BookEntity, Book>(_bookRepository.GetByCategory(category));
           
        }

        public IEnumerable<string> GetCategories()
        {
            return _bookRepository.GetAllCategories();
        }

        public IEnumerable<Book> Search(string searchText)
        {
            return _mapperService.Map<BookEntity, Book>(_bookRepository.Search(searchText));
        }
    }
}
