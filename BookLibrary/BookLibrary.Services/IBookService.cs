﻿using BookLibrary.Services.Models;
using System.Collections.Generic;

namespace BookLibrary.Services
{
    public interface IBookService
    {
        IEnumerable<Book> GetAllBooks();
        IEnumerable<Book> Search(string searchText);
        IEnumerable<Book> GetByCategory(string category);
        IEnumerable<string> GetCategories();
    }
}