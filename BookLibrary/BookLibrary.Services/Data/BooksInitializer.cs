﻿using BookLibrary.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Data
{
    public class BooksInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BooksContext>
    {
        protected override void Seed(BooksContext context)
        {

            //seed data from google books api
            var bookApi = new GoogleBooksApi.BooksApi();
            var result = bookApi.GetBooks(".net", 20).Result;

            var booksEntities = result.Select(b => new BookEntity
            {
                Title = b.title,
                Author = b.authors?.FirstOrDefault() ?? "",
                Category= b.categories?.FirstOrDefault() ?? "",
                Description = b.description == null ? "" : b.description


            }).ToList();

            booksEntities.ForEach(a => context.Books.Add(a));
            context.SaveChanges();
            
            
        }
    }
}
