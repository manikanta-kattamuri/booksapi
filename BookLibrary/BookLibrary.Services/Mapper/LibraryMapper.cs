﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Mapper
{
    public class LibraryMapper : IMapperService
    {
        public TDestination Map<TSource, TDestination>(TSource source)
            where TSource : class, new()
            where TDestination : class, new()
        {
            var dest = Activator.CreateInstance<TDestination>();
            return MatchAndMap(source, dest);
        }

        public IEnumerable<TDestination> Map<TSource, TDestination>(IEnumerable<TSource> source)
            where TSource : class, new()
            where TDestination : class, new()
        {
            return GetMapConfig()
                .CreateMapper()
                .Map<IEnumerable<TDestination>>(source);
        }
        private TDestination MatchAndMap<TSource, TDestination>(TSource source, TDestination destination)
            where TSource : class, new()
            where TDestination : class, new()
        {
            return GetMapConfig()
                .CreateMapper()
                .Map<TDestination>(source);
        }
        private MapperConfiguration GetMapConfig()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.AddProfile<BookMappings>();
                  
                });

            return config;
        }
    }
}
