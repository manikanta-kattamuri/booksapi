﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Mapper
{
    public interface IMapperService
    {
        TDestination Map<TSource, TDestination>(TSource source)
            where TSource : class, new()
            where TDestination : class, new();

        IEnumerable<TDestination> Map<TSource, TDestination>(IEnumerable<TSource> source)
            where TSource : class, new()
            where TDestination : class, new();
    }
}
