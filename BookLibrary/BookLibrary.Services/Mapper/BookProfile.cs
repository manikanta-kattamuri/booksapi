﻿using AutoMapper;
using BookLibrary.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Mapper
{
    public class BookMappings : Profile
    {
        public BookMappings()
        {
            CreateMap<BookEntity, Book>().ReverseMap();
        }
    }
}
