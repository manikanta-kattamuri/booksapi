﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookLibrary.Services.GoogleBooksApi;
using BookLibrary.Services.Mapper;
using BookLibrary.Services.Repository;
using StructureMap;
using StructureMap.Configuration.DSL;

namespace BookLibrary.Services.Registries
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            For<IMapperService>().Use<LibraryMapper>();
            //For<IBooksApi>().Use<BooksApi>();
            For<IBookRepository> ().Use<BookRepository> ();
            For<IBookService>().Use<BookService>();
        }
    }
}
