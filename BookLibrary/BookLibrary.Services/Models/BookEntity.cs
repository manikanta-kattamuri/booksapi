﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookLibrary.Services.Models
{
    public class BookEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Category { get; set; }

        public string Description { get; set; }
        

    }

}