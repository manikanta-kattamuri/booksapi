﻿using BookLibrary.Services.Data;
using BookLibrary.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Repository
{
    public interface IBookRepository
    {
        IEnumerable<BookEntity> FindAll();
        IEnumerable<BookEntity> Search(string searchText);
        IEnumerable<BookEntity> GetByCategory(string category);
        IEnumerable<string> GetAllCategories();
    }
}
