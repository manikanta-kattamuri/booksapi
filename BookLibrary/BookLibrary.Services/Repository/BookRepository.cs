﻿using BookLibrary.Services.Data;
using BookLibrary.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.Repository
{
    public class BookRepository : IBookRepository, IDisposable
    {
        private BooksContext _bookDbContext;

        public BookRepository()
        {
            _bookDbContext = new BooksContext();
        }

        public IEnumerable<BookEntity> FindAll()
        {

            return _bookDbContext.Books.AsEnumerable().ToList();

        }

        public IEnumerable<BookEntity> Search(string searchText)
        {
            return _bookDbContext.Books.AsEnumerable().Where(s => s.Author.ToUpper().Contains(searchText.ToUpper())
                                       || s.Category.ToUpper().Contains(searchText.ToUpper())
                                       || s.Description.ToUpper().Contains(searchText.ToUpper())
                                       || s.Title.ToUpper().Contains(searchText.ToUpper()));
        }

        public IEnumerable<string> GetAllCategories()
        {
            return _bookDbContext.Books.Select(c => c.Category).Distinct();
        }

        public bool Insert(BookEntity book)
        {
            _bookDbContext.Books.Add(book);
            return _bookDbContext.SaveChanges() > 0;
        }

        public IEnumerable<BookEntity> GetByCategory(string category)
        {
            return _bookDbContext.Books.Where(c => c.Category == category);
        }
        public void Dispose()
        {
            _bookDbContext.Dispose();
            _bookDbContext = null;
        }

        
    }
}
