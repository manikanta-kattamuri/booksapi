﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookLibrary.Services.GoogleBooksApi
{
    public interface IBooksApi
    {
        Task<IEnumerable<VolumeInfo>> GetBooks(string query, int maxResults);
    }
}