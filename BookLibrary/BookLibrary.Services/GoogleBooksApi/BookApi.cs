﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Services.GoogleBooksApi
{
    public class BooksApi
    {
        private static HttpClient client = new HttpClient();
       
        public async  Task<IEnumerable<VolumeInfo>> GetBooks(string query, int maxResults)
        {
            var apiResponse = new ApiResponse();
            HttpResponseMessage response = await client.GetAsync($"https://www.googleapis.com/books/v1/volumes?q={query}&fields=items(volumeInfo.authors,volumeInfo.title,volumeInfo.description,volumeInfo.categories)&maxResults={maxResults}").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                apiResponse = JsonConvert.DeserializeObject<ApiResponse>(data);
            }
            return ExtractBooksFrom(apiResponse);
        }

        private IEnumerable<VolumeInfo> ExtractBooksFrom(ApiResponse apiResponse)
        {
            return apiResponse.items.Select(a => a.volumeInfo).ToList();
        }
    }
}
