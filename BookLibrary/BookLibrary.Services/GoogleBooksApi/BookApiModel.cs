﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BookLibrary.Services.GoogleBooksApi
{
    public class ApiResponse
    {
        [JsonProperty(PropertyName = "items")]
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public VolumeInfo volumeInfo { get; set; }
    }

    public class VolumeInfo
    {
       
        public string title
        {
            get;
            set;
        }
       
        public string[] authors
        {
            get;
            set;
        }
      
        public string description
        {
            get;
            set;
        }
        
        public string[] categories
        {
            get;
            set;
        }
    }
}