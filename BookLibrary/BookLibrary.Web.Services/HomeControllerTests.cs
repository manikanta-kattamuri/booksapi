﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BookLibrary.Services;
using BookLibrary.Services.Models;
using BookLibrary.Web.Controllers;
using BookLibrary.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace BookLibrary.Web.Services
{
    
    public class HomeControllerTests
    {
        private readonly IBookService _bookService;
        private readonly HomeController _homeController;
        public HomeControllerTests()
        {
            _bookService = NSubstitute.Substitute.For<IBookService>();
            _homeController = new HomeController(_bookService);
        }

        [Test]
        public void HomeController_Index_Should_Return_View()
        {
            var mockBooks = new List<Book>();
            _bookService.GetAllBooks().Returns(mockBooks);

            var response = _homeController.Index();

            Assert.IsInstanceOf<ViewResult>(response);
            Assert.IsAssignableFrom<BookViewModel>((response as ViewResult).ViewData.Model);
        }

        [Test]
        public void HomeController_Search_Should_Return_View_With_Data()
        {
            var mockBooks = new List<Book>();
            _bookService.Search("test").Returns(mockBooks);
            _bookService.GetCategories().Returns(new List<string>() { "CatA","CatB"});

            var response = _homeController.Search(new BookViewModel() { SearchString = "test"});

            Assert.IsInstanceOf<ViewResult>(response);
            Assert.IsAssignableFrom<BookViewModel>((response as ViewResult).ViewData.Model);
            var model = (response as ViewResult).Model as BookViewModel;
            Assert.IsNotNull(model.Categories);
        }

        [Test]
        public void HomeController_Filter_Should_Return_View_With_Data()
        {
            var mockBooks = new List<Book>();
            _bookService.GetByCategory("test").Returns(mockBooks);
            _bookService.GetCategories().Returns(new List<string>() { "CatA", "CatB" });

            var response = _homeController.Filter(new BookViewModel() { SelectedCategory = "test" });

            Assert.IsInstanceOf<ViewResult>(response);
            Assert.IsAssignableFrom<BookViewModel>((response as ViewResult).ViewData.Model);
            var model = (response as ViewResult).Model as BookViewModel;
            Assert.IsNotNull(model.Categories);
        }
    }
}
